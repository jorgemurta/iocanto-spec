const Ajv = require("ajv");
const fs = require("fs");
const schema = require("../iocanto.schema.json");

async function loadFileContents(filePath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, { encoding: "utf-8" }, (err, data) => {
      if (err) {
        return reject(err);
      }

      resolve(data);
    });
  });
}

async function run() {
  process.on("unhandledRejection", (reason, p) => {
    console.error("Unhandled Rejection of Promise", p, "reason:", reason);
  });

  if (!process.argv[2]) {
    console.error(
      "Please provide a file name to process, like `iocanto-validator my/file.json`"
    );

    return;
  }

  const ajv = new Ajv();

  const validate = ajv.compile(schema);

  const fileContents = await loadFileContents(process.argv[2]);

  const valid = validate(JSON.parse(fileContents));
  if (!valid) console.log('Validation error:', validate.errors);
}

module.exports = {
  run
};
